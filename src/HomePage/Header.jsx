import React from 'react';
import { NavLink  } from 'react-router-dom';

import { Navbar, MenuItem, Nav, NavItem, NavDropdown  } from 'react-bootstrap';

export class Header extends React.Component {

    render() {
        const { user, users } = this.props;
        return (
            <Navbar inverse collapseOnSelect>
            <Navbar.Header>
                <Navbar.Brand>
                </Navbar.Brand>
                <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
                <Nav>
                    <NavItem eventKey={1} href="">
                        <NavLink  to="/signup">Signup</NavLink >
                    </NavItem>
                    <NavItem eventKey={2} href="">
                        <NavLink  to="/login">Login</NavLink >
                    </NavItem>
                    <NavItem eventKey={2} href="">
                        <NavLink  to="/most-liked">Most liked</NavLink >
                    </NavItem>
                </Nav>
            </Navbar.Collapse>
            </Navbar>
        );
    }
};