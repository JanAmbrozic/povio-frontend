import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../_actions';
import { ListGroup, ListGroupItem } from 'react-bootstrap';


class UsersPage extends React.Component {
    constructor(props) {
        super(props);
        this.props.dispatch(userActions.getAll())
    }

    render() {
        const { user, users } = this.props;
        return (
            <div className="col-md-6 col-md-offset-3">
                <h1>Most liked:</h1>
                {users.loading && <em>Loading users...</em>}
                {users.error && <span className="text-danger">ERROR: {users.error}</span>}
                {users.items &&
                    <ListGroup>
                        {users.items.map((user, index) =>
                            <ListGroupItem key={user.id} header={user.username}>
                                Number of likes: {user.likedByCount}
                            </ListGroupItem>
                        )}
                    </ListGroup>
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { users } = state;
    return {
        users
    };
}

const connectedUsersPage = connect(mapStateToProps)(UsersPage);
export { connectedUsersPage as UsersPage };

